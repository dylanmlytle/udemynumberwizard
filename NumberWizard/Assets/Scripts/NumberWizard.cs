﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour {

    int min;
    int max;
    int currentGuess;
    int roundCount;
    bool isGameFinished;

	// Use this for initialization
	void Start () {
        this.max = 1000;
        this.min = 1;
        this.currentGuess = max + min / 2;
        this.roundCount = 0;
        this.isGameFinished = false;

        print("Welcome to Number Wizard");
        print("Pick a number between " + min + " and " + max + "in your head, but don't tell me!");
        printNewRound();
	}
	
	// Update is called once per frame
	void Update () {
        if (!this.isGameFinished) {
            if (Input.GetKeyDown(KeyCode.UpArrow)) {
                this.min = currentGuess;
                printNewRound();
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow)) {
                this.max = currentGuess;
                printNewRound();
            }
            else if (Input.GetKeyDown(KeyCode.Return)) {
                print("I won in " + this.roundCount + " rounds!");
                this.isGameFinished = true;
            }
        }
    }

    void printNewRound()
    {
        updateCurrentGuess();
        this.roundCount++;
        print("ROUND " + this.roundCount + ": Is your number " + this.currentGuess + "?");
        print("Up = higher, Down = lower, return = equal");
    }

    void updateCurrentGuess()
    {
        this.currentGuess = (this.min + this.max) / 2;
    }
}
